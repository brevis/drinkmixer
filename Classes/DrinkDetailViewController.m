//
//  DrinkDetailViewController.m
//  DrinkMixer
//
//  Created by brevis on 10/11/14.
//
//

#import "DrinkDetailViewController.h"
#import "DrinkConstants.h"

@implementation DrinkDetailViewController

@synthesize drink=drink_, nameTextField=nameTextField_, ingredientsTextView=ingredientsTextView_, directionsTextView=directionsTextView_;
@synthesize scrollView=scrollView_;
@synthesize activeTextField=activeTextView_;

- (void)viewDidLoad {
    [super viewDidLoad];
	self.scrollView.contentSize = self.view.frame.size;
    
    self.ingredientsTextView.delegate = self;
    self.directionsTextView.delegate = self;
    
    // border for textView
    self.ingredientsTextView.layer.borderWidth = 0.5f;
    self.ingredientsTextView.layer.borderColor = [[UIColor grayColor] CGColor];
    self.ingredientsTextView.layer.cornerRadius = 8;
    self.directionsTextView.layer.borderWidth = 0.5f;
    self.directionsTextView.layer.borderColor = [[UIColor grayColor] CGColor];
    self.directionsTextView.layer.cornerRadius = 8;
    
    // add "Done" button to keyboard
    // TODO: think how to refactor it
    UIBarButtonItem *barButton1 = [[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self.ingredientsTextView action:@selector(resignFirstResponder)] autorelease];
    UIToolbar *toolbar1 = [[[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, 320, 44)] autorelease];
    toolbar1.items = [NSArray arrayWithObject:barButton1];
    self.ingredientsTextView.inputAccessoryView = toolbar1;

    UIBarButtonItem *barButton2 = [[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self.directionsTextView action:@selector(resignFirstResponder)] autorelease];
    UIToolbar *toolbar2 = [[[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, 320, 44)] autorelease];
    toolbar2.items = [NSArray arrayWithObject:barButton2];
    self.directionsTextView.inputAccessoryView = toolbar2;

    UIBarButtonItem *barButton3 = [[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self.nameTextField action:@selector(resignFirstResponder)] autorelease];
    UIToolbar *toolbar3 = [[[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, 320, 44)] autorelease];
    toolbar3.items = [NSArray arrayWithObject:barButton3];
    self.nameTextField.inputAccessoryView = toolbar3;
    
    self.activeTextField = nil;
}

- (void)viewWillAppear:(BOOL)animated {
	[super viewWillAppear:animated];
	
	self.nameTextField.text = [self.drink objectForKey:NAME_KEY];
	self.ingredientsTextView.text = [self.drink objectForKey:INGREDIENTS_KEY];
	self.directionsTextView.text = [self.drink objectForKey:DIRECTIONS_KEY];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidShow:) name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidHide:) name:UIKeyboardDidHideNotification object:nil];
    
    keyboardVisible_ = NO;

}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}


- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


- (void)dealloc {
   	[activeTextView_ release];
	[nameTextField_ release];
	[ingredientsTextView_ release];
	[directionsTextView_ release];
	[scrollView_ release];
	[drink_ release];
	
    [super dealloc];
}

#pragma mark -
#pragma mark Keyboard handlers

- (void) keyboardDidShow:(NSNotification *)notif {
    if (keyboardVisible_) return;
    
    NSDictionary* info = [notif userInfo];
    kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    keyboardVisible_ = YES;
    self.scrollView.scrollEnabled = true;
    [self scrollContent];
}

- (void) keyboardDidHide:(NSNotification *)notif {
    if (!keyboardVisible_) return;
    keyboardVisible_ = NO;
    [self.scrollView setContentOffset:CGPointMake(0, -50) animated:YES];
    self.scrollView.scrollEnabled = false;
}

- (void) scrollContent {
    if (keyboardVisible_ && self.activeTextField != nil) {
        float padding = 10;
        float maxAvailableY = self.view.frame.size.height - self.navigationController.navigationBar.frame.size.height - self.navigationController.navigationBar.frame.origin.y - kbSize.height;
        float textViewBottom = self.activeTextField.frame.origin.y + self.activeTextField.frame.size.height - self.navigationController.navigationBar.frame.size.height - self.navigationController.navigationBar.frame.origin.y + padding;

        [self.scrollView setContentOffset:CGPointMake(0, textViewBottom - maxAvailableY) animated:YES];
    }
}

#pragma - mark TextView Delegate Methods

- (void)textViewDidBeginEditing:(UITextView *)textView {
    self.activeTextField = textView;
    [self scrollContent];
}

- (void)textViewDidEndEditing:(UITextView *)textView{
    self.activeTextField = nil;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
