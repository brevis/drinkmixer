//
//  DrinkConstants.h
//  DrinkMixer
//
//  Created by brevis on 10/11/14.
//
//

#define NAME_KEY @"name"
#define INGREDIENTS_KEY @"ingredients"
#define DIRECTIONS_KEY @"directions"
#define IMAGE_KEY @"imageUrl"