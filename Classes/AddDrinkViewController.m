//
//  AddDrinkViewController.m
//  DrinkMixer
//
//  Created by brevis on 10/11/14.
//
//

#import "AddDrinkViewController.h"
#import "DrinkConstants.h"


@implementation AddDrinkViewController

@synthesize drinkArray=drinkArray_;

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.leftBarButtonItem = [[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(cancel:)] autorelease];
    self.navigationItem.rightBarButtonItem = [[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemSave target:self action:@selector(save:)] autorelease];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc {
    [drinkArray_ release];
    [super dealloc];
}

#pragma mark -
#pragma mark - Actions

- (void)save: (id)sender {
    
    if (self.drink != nil) {
        [drinkArray_ removeObject:self.drink];
        self.drink = nil;
    }
    
    NSMutableDictionary *newDrink = [[NSMutableDictionary alloc] init];
    [newDrink setValue:self.nameTextField.text forKey:NAME_KEY];
    [newDrink setValue:self.ingredientsTextView.text forKey:INGREDIENTS_KEY];
    [newDrink setValue:self.directionsTextView.text forKey:DIRECTIONS_KEY];
    [newDrink setValue:@"" forKey:IMAGE_KEY];
    [drinkArray_ addObject:newDrink];
    [newDrink release];
    
    NSSortDescriptor *nameSorter = [[NSSortDescriptor alloc] initWithKey:NAME_KEY ascending:YES selector:@selector(caseInsensitiveCompare:)];
    [drinkArray_ sortUsingDescriptors:[NSArray arrayWithObjects:nameSorter, nil]];
    [nameSorter release];
    
    [self dismissViewControllerAnimated:YES completion:nil];
}


-(IBAction)cancel:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
