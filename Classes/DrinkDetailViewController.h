//
//  DrinkDetailViewController.h
//  DrinkMixer
//
//  Created by brevis on 10/11/14.
//
//

#import <UIKit/UIKit.h>

@interface DrinkDetailViewController : UIViewController <UITextViewDelegate> {

@private
	UITextField *nameTextField_;
	UITextView *ingredientsTextView_;
	UITextView *directionsTextView_;
	UIScrollView *scrollView_;
	NSDictionary *drink_;
    BOOL keyboardVisible_;
    UITextView *activeTextView_;
    CGSize kbSize;
}

@property (nonatomic, retain) IBOutlet UITextField *nameTextField;
@property (nonatomic, retain) IBOutlet UITextView *ingredientsTextView;
@property (nonatomic, retain) IBOutlet UITextView *directionsTextView;
@property (nonatomic, retain) IBOutlet UIScrollView *scrollView;
@property (nonatomic, retain) NSDictionary *drink;
@property (nonatomic, assign) UITextView *activeTextField;

- (void) textViewDidBeginEditing: (UITextView *) textView;
- (void) textViewDidBeginEditing: (UITextView *) textView;
- (void) keyboardDidShow:(NSNotification *)notif;
- (void) keyboardDidHide:(NSNotification *)notif;

@end