//
//  AddDrinkViewController.h
//  DrinkMixer
//
//  Created by brevis on 10/11/14.
//
//

#import "DrinkDetailViewController.h"

@interface AddDrinkViewController : DrinkDetailViewController {   
    NSMutableArray *drinkArray_;
}

@property (nonatomic, retain) NSMutableArray *drinkArray;

- (IBAction) save: (id) sender;
- (IBAction) cancel: (id) sender;

@end
